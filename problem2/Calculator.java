package junitdxc;

public class Calculator {

	 public static double calculateSimpleInterest(double principal, double rate, double time) 
	            throws InvalidPrincipalException, InvalidRateException, InvalidTimeException {
	        
	        if (principal <= 0) {
	            throw new InvalidPrincipalException("Invalid principal: " + principal);
	        }
	        
	        if (rate <= 0) {
	            throw new InvalidRateException("Invalid rate: " + rate);
	        }
	        
	        if (time <= 0) {
	            throw new InvalidTimeException("Invalid time: " + time);
	        }
	        
	        double simpleInterest = principal * rate * time;
	        
	        return simpleInterest;
	    }
	    
	}

	 class InvalidPrincipalException extends Exception {
	    public InvalidPrincipalException(String message) {
	        super(message);
	    }
	}

	 class InvalidRateException extends Exception {
	    public InvalidRateException(String message) {
	        super(message);
	    }
	}

	 class InvalidTimeException extends Exception {
	    public InvalidTimeException(String message) {
	        super(message);
	    }
	}

