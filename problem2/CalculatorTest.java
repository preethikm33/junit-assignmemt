package junitdxc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

 public class CalculatorTest {

	@Test(expected = InvalidPrincipalException.class)
    public void testInvalidPrincipal() throws InvalidPrincipalException, InvalidRateException, InvalidTimeException {
        Calculator.calculateSimpleInterest(0, 5, 10);
    }
    
    @Test(expected = InvalidRateException.class)
    public void testInvalidRate() throws InvalidPrincipalException, InvalidRateException, InvalidTimeException {
        Calculator.calculateSimpleInterest(1000, 0, 10);
    }
    
    @Test(expected = InvalidTimeException.class)
    public void testInvalidTime() throws InvalidPrincipalException, InvalidRateException, InvalidTimeException {
        Calculator.calculateSimpleInterest(1000, 5, 0);
    }
    
    @Test
    public void testValidInputs() throws InvalidPrincipalException, InvalidRateException, InvalidTimeException {
        double expectedSimpleInterest =40000;
        double actualSimpleInterest = Calculator.calculateSimpleInterest(5000, 4, 2);
        assertEquals(expectedSimpleInterest, actualSimpleInterest, 0.0001);
    }
}
	