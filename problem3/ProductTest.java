package junitdxc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

 public class ProductTest {
	@Test
    public void testCalculateFinalPriceWithDiscount() {
        // create a new Product with a price of 10 and a discount of 25%
        Product product = new Product();
        product.setPrice(10);
        float finalPrice = product.calculateFinalPrice(25);

        // assert that the final price is equal to 7.5 with a tolerance of 0
        assertEquals(7.5, finalPrice, 0);
    }

    @Test
    public void testCalculateFinalPriceWithNoDiscount() {
        // create a new Product with a price of 10 and no discount
        Product product = new Product();
        product.setPrice(10);
        float finalPrice = product.calculateFinalPrice(0);

        // assert that the final price is equal to 10 with a tolerance of 0
        assertEquals(10, finalPrice, 0);
    }

    @Test
    public void testCalculateFinalPriceWithNegativeDiscount() {
        // create a new Product with a price of 10 and a discount of -25%
        Product product = new Product();
        product.setPrice(10);
        float finalPrice = product.calculateFinalPrice(-25);

        // assert that the final price is equal to 12.5 with a tolerance of 0
        assertEquals(12.5, finalPrice, 0);
    }
}

	


